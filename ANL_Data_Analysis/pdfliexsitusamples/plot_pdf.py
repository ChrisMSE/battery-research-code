import pandas as pd
import matplotlib.pyplot as plt

def plot_files(files):
    fig = plt.figure()
    for File in files:
        file_name = '{}.csv'.format(File)
        csv_file = pd.read_csv(file_name)
        x = csv_file['x']
        y = csv_file['y_obs']
        label = File
        plt.plot(x,y,label=label)


    plt.show()
