import pandas as pd
import numpy as np
import spacing as spc
from pandas import ExcelFile
from pandas import ExcelWriter
import matplotlib.pyplot as plt


def convert_data(file,llambda_in,llambda_out):

    # llambda_in = wavelength used to collect data
    # llambda_out = wavelength desired to plot data
    
    data_file = pd.read_csv(file)
    twoTheta = np.array(data_file['x'])
    intensity = np.array(data_file['y_obs'])

    twoTheta_d = spc.theta_convert(twoTheta,llambda_in)
    twoTheta_new = spc.d_convert(twoTheta_d,llambda_out)

    df = pd.DataFrame({'x':twoTheta_new,'y_obs':intensity})
  
    path = '{}_converted.csv'.format(file.split('.')[0])

    df.to_csv(path,index=False)

    new_file = pd.read_csv(path)
    x = np.array(new_file['x'])
    y = np.array(new_file['y_obs'])

    return x,y


x,y = convert_data('Nb_TiO2_D20_1st-Li-00018_B11_bcksub.csv',0.11733,0.24125)

plt.plot(x,y)
plt.show()



    

