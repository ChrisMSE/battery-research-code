import os
import plot_data as plot

phases = ['Anatase','Rutile','Both','None']
back_sub_files = ['Li_C_20_1st.csv','Li_D_20_1st.csv','Li_D_175_1st.csv','Li_D_178_1st.csv']

print('What do you want to plot?')
print('1) Exsitu XRD')
print('2) Exsitu PDF')
print('3) Powder XRD')
print('4) Powder PDF')
plot_type = int(input())

if plot_type == 1:
    path = 'Exsitu/XRD'
    bkg_file = 'C45_pvdf_ref_xrd.csv'
elif plot_type == 2:
    path = 'Exsitu/PDF'
    bkg_file = 'C45_pvdf_ref_pdf.csv'
elif plot_type == 3:
    path = 'Powder/XRD'
elif plot_type == 4:
    path = 'Powder/PDF'

os.chdir(path)

curr_dir = []
for f in os.listdir():
    if f.split('.')[1] == 'csv':
        curr_dir.append(f)
    else: pass
curr_dir = sorted(curr_dir)
print()
print('FILES:')
for n in range(0,len(curr_dir)):
    print('{}) {}'.format((n+1),curr_dir[n]))

print()
print('Enter which files you want to plot. Separate with a comma (no spaces)')
ans = input()
samples = [int(i) for i in ans.split(',')]

print()
if plot_type == 1 or plot_type == 3:
    print('What phase(s) do you want indicated?')
    for n in range(0,len(phases)):
        print('{}) {}'.format((n+1),phases[n]))

    phase = int(input())
    standard_phase = phases[phase-1]

    print('Do you want to plot the pristine sample? [y/n]')
    ans = input()
    if ans == 'y' or ans == 'Y' or ans == 'yes' or ans == 'Yes':
        pristine = 'true'
    else:
        pristine = 'false'

if plot_type == 1:
    plot.xrd(files=curr_dir,samples=samples,background=bkg_file,standard=standard_phase,llambda=1,pristine=pristine)
elif plot_type == 2:
    plot.pdf(files=curr_dir, samples=samples, background=bkg_file)
