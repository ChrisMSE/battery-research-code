import numpy as np
import matplotlib.pyplot as plt
import spacing as space

def anatase(spacing):
    theta_peaks = [25.3,36.9,37.8,38.6,48.0,53.9,55.1,62.1,62.7,68.8,70.3,74.1,75.0,76.1]
    #d_peaks = [1.8024,1.2829,1.2568,1.2347,1.0365,0.9534,0.9392,0.8716,0.8669,0.8262,0.8182,0.8009,0.7975,0.7935]
    d_peaks = [3.51, 2.44, 2.38, 2.33, 1.89, 1.70, 1.67, 1.49, 1.48, 1.36, 1.34, 1.28, 1.27, 1.25]
    
    if spacing == 'theta':
        peaks = np.array(theta_peaks)
    elif spacing == 'd':
        peaks = np.array(d_peaks)

    return peaks

def rutile(spacing):
    theta_peaks = [28.0,36.6,39.8,41.8,44.6,54.9,57.2]
   # d_peaks = [1.6408,1.2889,1.2034,1.1557,1.0971,0.9415,0.9164]
    d_peaks = [3.18, 2.45, 2.26, 2.16, 2.03, 1.67, 1.61]
    if spacing == 'theta':
        peaks = np.array(theta_peaks) 
    elif spacing == 'd':
        peaks = np.array(d_peaks)

    return peaks

