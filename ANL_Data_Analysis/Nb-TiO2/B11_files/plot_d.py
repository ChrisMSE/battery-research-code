import matplotlib.pyplot as plt
import pandas as pd
import spacing as spc
import standards as std

def plot_d(files):
    for F in files:
        file_name = '{}.csv'.format(F)
        csv_file = pd.read_csv(file_name)
        two_theta = csv_file['x']
        y = csv_file['y_obs']
        d_space = spc.theta_convert(two_theta,0.11733)

        plt.plot(d_space,y)
        plt.xlabel('d spacing')

    plt.show()

files = ['Li_C_20_bsub']
plot_d(files)
