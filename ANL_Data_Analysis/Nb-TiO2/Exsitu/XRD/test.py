import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

bkg = pd.read_csv('C45_pvdf_ref_xrd.csv')
sample = pd.read_csv('Na_D_05_2nd.csv')

x_bkg = np.array(bkg['x'])
y_bkg = np.array(bkg['y_obs'])

x = np.array(sample['x'])
y = np.array(sample['y_obs'])

print(len(x_bkg))
print(len(x))

fig = plt.figure()
plt.plot(x_bkg,y_bkg)
plt.plot(x,y)
plt.show()
