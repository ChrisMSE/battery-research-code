import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt
import pandas as pd
import background as bkg
import standards as std
import spacing as spc

# samples = list of numbers (index pos) of files to be plotted
# files = list of all file names in the cwd
# background = relevant background file for subtraction

back_sub_files = ['Li_C_20_1st.csv','Li_D_20_1st.csv','Li_D_175_1st.csv','Li_D_178_1st.csv']

def pdf(files,samples,background):
    wavelengths = []
    distances = []
    for s in samples:
        sample_file = files[s-1]
        csv_file = pd.read_csv(sample_file)
        x = csv_file['x']
        y = csv_file['y_obs']
        
        dist,llambda = read_meta(sample_file)
        dist = float(dist)
        llambda = float(llambda)
        if dist < 300:
            pass
        elif dist > 300:
            print('{} appears to be an XRD data file'.format(sample_file))
        if llambda > 0.20:
            beam_line = 17
        elif llambda < 0.20:
            beam_line = 11
            x_d = spc.theta_convert(x,0.11733)
            x = spc.d_convert(x_d,0.24125)
            y = y/2

        ysub = bkg.subtract(sample_file,background,xrd='false')    
        wavelengths.append(llambda)
        distances.append(dist)
        label = sample_file.split('.')[0]
        plt.plot(x,ysub,label=label)

    plt.title('PDF')
    plt.legend(loc='best')
    plt.xlabel('r (A)')
    plt.ylabel('AU')
    plt.show()


def xrd(files,samples,background,standard,llambda,pristine):
    wavelengths = []
    distances = []

    if pristine == 'true':
        fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, sharey=False)
        axes[0].set_title('Prisitne Sample')
    else:
        pass
    
    for s in samples:
        sample_file = files[s-1]
        csv_file = pd.read_csv(sample_file)
        x = csv_file['x']
        y = csv_file['y_obs']
        
##        dist,llambda = read_meta(sample_file)
##        dist = float(dist)
##        llambda = float(llambda)
##        if llambda < .20:
##            beam_line = 11
##        else:
##            beam_line = 17
            
##        if dist > 300:
##            pass
##        elif dist < 300:
##            print('{} appears to be a PDF data file'.format(sample_file))
##        if llambda > 0.20:
##            pass
##        elif llambda < 0.20:
##            x_d = spc.theta_convert(x,0.11733)
##            x = spc.d_convert(x_d,0.24125)
            
        if sample_file in back_sub_files:
            ysub = y/6
            ysub = ysub - 400
            xsub = x
        else:
            ysub = bkg.subtract(sample_file,background,xrd='true')
            xsub = [x[n] for n in range(0,len(ysub))]
        label = sample_file.split('.')[0]
        
        if pristine == 'true':
            axes[1].plot(xsub,ysub,label=label)
        else:
            plt.plot(xsub,ysub,label=label)
            
        plt.legend(loc='best')

       
    if standard == 'Anatase':
        peaks = std.anatase('d')
        peaks_th = spc.d_convert(peaks,0.24125)
        peaks_th_y = [-350 for i in peaks_th]
        plt.plot(peaks_th,peaks_th_y,'r|',label='Anatase')
	    
    elif standard == 'Rutile':
        peaks = std.rutile('d')
        peaks_th = spc.d_convert(peaks,0.24125)
        peaks_th_y = [-350 for i in peaks_th]
        plt.plot(peaks_th,peaks_th_y,'k|',label='Rutile')
            
    elif standard == 'Both':
        peaks1 = std.anatase('d')
        peaks2 = std.rutile('d')
        peaks1_th = spc.d_convert(peaks1,0.24125)
        peaks1_th_y = [-350 for i in peaks1_th]
        plt.plot(peaks1_th,peaks1_th_y,'r|',label='Anatase')

        peaks2_th = spc.d_convert(peaks2,0.24125)
        peaks2_th_y = [-350 for i in peaks2_th]
        plt.plot(peaks2_th,peaks2_th_y,'k|',label='Rutile')

    elif standard == 'None':
        pass

    if pristine == 'true':
        pristine_file = pd.read_csv('pristine_xrd2.csv')
        prisx = pristine_file['x']
        prisy = pristine_file['y_obs']
        prisx_d = spc.theta_convert(prisx,0.11733)
        prisx_th = spc.d_convert(prisx_d,0.24125)
        
        axes[0].plot(prisx_th,prisy,label='Pristine')
        anatase_peaks = std.anatase('d')
        anatase_peaks = spc.d_convert(anatase_peaks,0.24125)
        peaks_y = [10 for i in anatase_peaks]
        axes[0].plot(anatase_peaks,peaks_y,'r|',label='Anatase')
                   
        axes[0].legend(loc='best')
        axes[0].set_yticklabels([])
        
    plt.title('XRD')
    plt.legend(loc='best')
    plt.xlabel('2θ')
    plt.yticks([])
    plt.show()


def read_meta(sample_file):
    file_name = sample_file.split('.')[0]
    meta_file = open('{}.tif.metadata'.format(file_name),'r')
    lines = meta_file.read().splitlines()
    
    for line in lines: #meta_file:
        if line.split('=')[0] == 'Distance':
            distance = line.split('=')[1]
        elif line.split('=')[0] == 'Wavelength':
            llambda = (line.split('=')[1])
        else: pass

    return distance,llambda
