import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import standards as std
import spacing as sp

def subtract(sample,reference,xrd):
    data_file = pd.read_csv(sample)
    bkg_file = pd.read_csv(reference)

    if xrd == 'true':
        x_data = np.array(data_file['x'])
        y_data = np.array(data_file['y_obs'])
        x_bkg = np.array(bkg_file['x'])
        y_bkg = np.array(bkg_file['y_obs'])

        shift = 100
        y_sub = []
        for i in range(0,len(x_bkg)-shift):
            diff = y_data[i] - y_bkg[i+shift]
            y_sub.append(diff)
    
    else:
        x_data = np.array(data_file['x'])
        y_data = np.array(data_file['y_obs'])
        x_bkg = np.array(bkg_file['x'])
        y_bkg = np.array(bkg_file['y_obs'])

        if len(x_data) != len(x_bkg):
            print('THE SAMPLE AND BACKGROUND ARE NOT THE SAME SIZE!')
        y_sub = []
        for i in range(0,len(x_bkg)):
                diff = y_data[i] - y_bkg[i]
                if diff > 0:
                    y_sub.append(diff)
                else:
                    y_sub.append(diff)
                   # y_sub.append(y_sub[i-1])

    return y_sub


