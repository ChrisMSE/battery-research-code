import numpy as np
import os

os.chdir('Exsitu/XRD')
sample = 'Li_C_18_1st'
meta_file = open('{}.tif.metadata'.format(sample),'r')

for line in meta_file:
    if line.split('=')[0] == 'Distance':
        print('{} = {}'.format(line.split('=')[0],line.split('=')[1]))
    elif line.split('=')[0] == 'Wavelength':
        print('{} = {}'.format(line.split('=')[0],line.split('=')[1]))
    else:
        pass

