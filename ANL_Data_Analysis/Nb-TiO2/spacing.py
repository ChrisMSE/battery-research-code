import math
import numpy as np

def theta_convert(array,llambda): # Input 2theta values array, output d spacing array
    # llambda is the wavelength used to collect the 2theta values of the input array
    
    theta_deg = array/2  # Theta in deg
    d_spacing = []
    for n in theta_deg:
        sin_theta = math.sin(math.radians(n))
        d = ((llambda)/(2*sin_theta))
        d_spacing.append(d)

    return d_spacing

def d_convert(array, llambda):  # Input d spacing values array, output 2theta array
    # llambda parameter is the desired llambda for the 2theta values

    two_theta = []
    for d in array:
        x = llambda/(2*d)
        theta = math.asin(x)
        theta_deg = math.degrees(theta)
        two_theta.append(2*theta_deg)

    return two_theta

