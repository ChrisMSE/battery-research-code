import unittest
import Battery_Data_Module
import pandas as pd
from Battery_Data_Module import File

# Create a test class
# Inherit from unittest

class TestArbin(unittest.TestCase):

    def test_check_file(self):
        self.assertEqual(Battery_Data_Module.check_file('unit_test'), 'unit_test.xls')
        self.assertEqual(Battery_Data_Module.check_file('no_file'), 0)
        self.assertEqual(Battery_Data_Module.check_file('unit_test_dup'), 1)

    def test_column_info(self):
        pass

    def test_individual_cycles(self):
        pass

class TestFile(unittest.TestCase):
    
    def test_battery_type(self):
        file = File('unit_test.xls')
        self.assertEqual(file.battery_type, 'half cell')
        
        



#####
        
if __name__ == '__main__':
    unittest.main()

        
