# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary:
This python script is meant to be used with the Excel files created by the Arbin cycling machine.  With this script you can create voltage profile graphs, dQ/dV graphs, and capacity vs. cycle graphs (Rate study, cycle life, etc...)

In addition to graphing the data, new Excel spreadsheets will be created containing only the data you are interested in.  This will make it much easier to capture a small portion of data, and allows you to easily create graphs in other programs such as Oriin as needed.

* Version
2018.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
To run this program you need the following:
	-Python 3.6
	-Additional Python Modules:
		-Matplotlib
		-Numpy
		-Pandas
		-Scipy
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
