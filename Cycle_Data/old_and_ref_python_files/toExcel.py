def inExcel(cycles,charge,discharge):
    
    if charge == 'True':
        #charge_cycles = charge_cycles
        #charge_capacity = charge_capacity
        #charge_volts = charge_volts
        for i in cycles:
            work_sheet_name = 'Cycle{}'.format(i)
            index = range(0,len(charge_volts))
            df=pd.DataFrame({'Cycle (Charge)':charge_cycles,'Charge Voltage':charge_volts, 'Charge Specific Capacity':charge_capacity},index=index,columns=['Cycle (Charge)','Charge Voltage','Charge Specific Capacity'])
            df.to_excel(writer, sheet_name = work_sheet_name)
        
    elif discharge == 'True':
        #discharge_cycles = discharge_cycles
        #discharge_capacity = discharge_capacity
        #discharge_volts = discharge_volts
        for i in cycles:
            work_sheet_name = 'Cycle{}'.format(i)
            index = range(0,len(discharge_volts))
            df=pd.DataFrame({'Cycle (Discharge)':discharge_cycles,'Discharge Voltage':discharge_volts, 'Discharge Specific Capacity':discharge_capacity},index=index,columns=['Cycle (Discharge)','Discharge Voltage','Discharge Specific Capacity'])
            df.to_excel(writer, sheet_name = work_sheet_name)
        
    elif charge == 'True' and discharge == 'True'
        if len(discharge_volts) > len(charge_volts):
            index = range(0,len(discharge_volts))
        else:
            index = range(0,len(charge_volts))
            
        for i in cycles:
            work_sheet_name = 'Cycle{}'.format(i)
            df=pd.DataFrame({'Cycle (Charge)':charge_cycles,'Charge Voltage':charge_volts, 'Charge Specific Capacity':charge_capacity,'Cycle (Discharge)':discharge_cycles,'Discharge Voltage':discharge_volts, 'Discharge Specific Capacity':discharge_capacity},index=index,columns=['Cycle (Charge)','Charge Voltage','Charge Specific Capacity','Cycle (Discharge)','Discharge Voltage','Discharge Specific Capacity'])
            df.transpose()
            df.to_excel(writer, sheet_name = work_sheet_name)
        

    writer.save()
