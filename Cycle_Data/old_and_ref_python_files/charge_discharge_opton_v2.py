import sys 
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import os as os
from os import path
from textwrap import wrap
import shutil as shutil
import random as r

######################### PRELIMINARY/HOUSE KEEPING ###########################
def get_set_up():

    print("Type the name of the file you want to import (Caps sensitive)")
    filename = input()	
    filename = filename.split(".")[0]
    
    print("")
    print('Type the active weight of your sample (in grams):')
    aw = float(input())
    print("")
    if os.path.isfile(filename+str(".xlsx")) is True:
        data_file =(filename+'.xlsx') #adds the correct file extension needed 
							    #for the variable data
    else:
        data_file =(filename+'.xls')
        print("...Loading the spreadsheet - This might take awhile!...")
        data = pd.ExcelFile(data_file) #Opens the excel file

	#Creates a folder to save graphs and excel spreadsheets:

    dir = (filename + str(" Data"))
    if os.path.exists(dir):
        shutil.rmtree(dir)
    os.makedirs(dir)

	#Open the stats worksheet to determine the total number of cycles:

    stats = pd.read_excel(data,sheetname = -1)
    information = pd.read_excel(data,sheetname=0,skiprows=3)
    total_cycles = np.array(stats['Cycle_Index'])

	#Gets a list of sheet names to determine how many sheets contian data:

    comments = np.array(information['Comments'])
    channel = np.array(information['Channel'])
    sheets = (data.sheet_names)
    sheet_num = len(data.sheet_names) - 1
    
    print("")
    print("")
    print("BATTERY INFORMATION:")
    print("--------------------")
    print("Cycles Completed: "+str(total_cycles[-1]))
    print("Channel: " + str(channel))
    print("Comments: " + str(comments))
    print("--------------------")
    print("")

    return total_cycles, sheet_num, aw, filename, dir, data, stats

###############################################################################


############### GRABS CYCLE DATA FROM THE ARBIN SPREADSHEET ###################

def get_column_info(column_name,index,omit_column):
    currIndex = 0
    retVal = []
        
    for i in range(1,sheet_num):
        if(currIndex>index):break
	
        ws = pd.read_excel(data,sheetname=i)
        volt = np.array(ws[column_name])
        omit = np.array(ws[omit_column])
        index_list = np.array(ws["Cycle_Index"])
        n = 0
        
        while(currIndex <= index and n < len(index_list)):
            if(index_list[n] == index) and omit[n] == 0:
                retVal.append(volt[n])
            currIndex = index_list[n]
            n = n + 1
            
    return retVal

    # All of the functions below rely on and use this
    # function to return the needed data  #

###############################################################################

def skip_cycle():
    
    print("")
    print("Do you want to omit any cycles before graphing the data? [y/n]")
    answer = input()
    answer_low = answer.lower()
    
    if answer_low == 'n' or answer_low == 'no':
        skip_cycles = []
        
    elif answer_low == 'y' or answer_low == 'yes':
        print("")
        print("Enter the cycle number(s) you want to omit (Separate each with a comma if there is more than one).")
        skip_cycles = [int(x) for x in input().split(",")]
        
    else:
        print("That wasn't a valid choice. Please select yes or no [y/n]")
    
    return skip_cycles

def get_capacity(column_name):
    
    retVal = []

    if skip_cycles == []:
        values = stats[column_name]
        retVal.append(values)
        
    else:
        values = stats[column_name]
        for i in skip_cycles:
            del values[i - 1]
            new_values = values
        retVal.append(new_values)
        
    return retVal

################### 4 DIFFERENT POSSIBLE PLOTTING FUNCTONS ####################

def plot_capacity():

    #Define the needed variables#
    cycle = np.array(get_capacity('Cycle_Index'))
    charge_capacity = np.array(get_capacity('Charge_Capacity(Ah)'))
    discharge_capacity = np.array(get_capacity('Discharge_Capacity(Ah)'))
    charge_specific_capacity = (charge_capacity) * (1000/aw)
    discharge_specific_capacity = (discharge_capacity) * (1000/aw)
    col_eff = ((charge_specific_capacity)/(discharge_specific_capacity)) * 100

   #Flatten the arrays for graphing#
    cycle_flat = cycle.flatten()
    charge_specific_capacity_flat = charge_specific_capacity.flatten()
    discharge_specific_capacity_flat = discharge_specific_capacity.flatten()
    col_eff_flat = col_eff.flatten()

    graphtitle_speccap = filename+str('_specific_capacity_graph.png')
    graph_path_speccap = path.join(dir,graphtitle_speccap)
    graphtitle_charge = filename+str('_charge_capacity.png')
    graph_path_charge = path.join(dir,graphtitle_charge)
    graphtitle_discharge = filename+str('_discharge_capacity.png')
    graph_path_discharge = path.join(dir,graphtitle_discharge)
    exceltitle = filename+str('_data.xlsx')
    excelpath = path.join(dir,exceltitle)

    #Specific Capacity vs Cycle#
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.plot(cycle_flat,charge_specific_capacity_flat,'go',label='Charge')
    ax.plot(cycle_flat,discharge_specific_capacity_flat,'ro',label='Discharge')
    plt.title('Specific Capacity vs Cycle')
    plt.ylabel ('Specific Capacity (mAh/g)')
    plt.xlabel('Cycle Number')
    plt.legend(loc='upper right',shadow=True, fancybox=True, numpoints=1)
    plt.savefig(graph_path_speccap)

#Charge Specific Capacity & Coulombic Eff#
    fig2, ax1 = plt.subplots()
    ax1.set_title('Charge Capacity vs Cycle')
    ax1.set_xlabel('Cycle Number')
    ax1.set_ylabel('Specific Capacity (mAh/g)',color='green')
    ax1.plot(cycle_flat,charge_specific_capacity_flat,'g^')
    ax1.tick_params(axis='y',labelcolor='green')

                #Initiate the 2nd Y axis:
    ax2 = ax1.twinx()
    ax2.set_ylabel('Coulombic Efficiency (%)',color='blue')
    ax2.plot(cycle_flat,col_eff_flat,'bo')
    ax2.tick_params(axis='y', labelcolor='blue')
    fig2.tight_layout()
    plt.savefig(graph_path_charge)

#Discharge Specific Capacity & Coulombic Eff#
    fig3, ax1 = plt.subplots()
    ax1.set_title('Discharge Capacity vs Cycle')
    ax1.set_xlabel('Cycle Number')
    ax1.set_ylabel('Specific Capacity (mAh/g)',color='red')
    ax1.plot(cycle_flat,discharge_specific_capacity_flat,'r^')
    ax1.tick_params(axis='y',labelcolor='red')

                #Initiate the 2nd Y axis:
    ax2 = ax1.twinx()
    ax2.set_ylabel('Coulombic Efficiency (%)',color='blue')
    ax2.plot(cycle_flat,col_eff_flat,'bo')
    ax2.tick_params(axis='y', labelcolor='blue')
    fig2.tight_layout()
    plt.savefig(graph_path_discharge)
    
    #Saves the data to a new excel file:
    df=pd.DataFrame({'Cycle':cycle_flat, 'Charge Specific Capacity':charge_specific_capacity_flat, 'Discharge Specific Capacity':discharge_specific_capacity_flat, 'Coulombic Efficiency':col_eff_flat},columns=['Cycle','Charge Specific Capacity','Discharge Specific Capacity','Coulombic Efficiency'])
    writer = pd.ExcelWriter((excelpath), engine='xlsxwriter')
    df.to_excel(writer, sheet_name = filename)
    writer.save()

    print("")
    print("Finished!")
    print("")

def plot_all(charge_type):  #Plots all of the cycles recorded on the stats worksheet
    fig = plt.figure()
    ax = plt.subplot(111)
    plt.suptitle('Voltage vs Specific Capacity',fontsize=20)
    plt.title ('All Cycles',fontsize='smaller')
    plt.ylabel ('Voltage (V)')
    plt.xlabel ('Specific Capacity (mAh/g)')
    graph_filename = filename + str('_all_cycles.png')
    graph_path = path.join(dir,graph_filename)

    print("")
    print("Plotting all cycles can take a long time. Please be patient!")
    print("")

    if charge_type == 'both':
        for i in total_cycles:
            volts = get_column_info('Voltage(V)',i)
            spec_cap_charge = get_column_info('Charge_Capacity(Ah)',i)
            spec_cap_dis = get_column_info('Discharge_Capacity(Ah)',i)
            
            for i in range(0,len(spec_cap_charge)):
                spec_cap_charge[i] = spec_cap_charge[i]*1000/aw
                spec_cap_dis[i] = spec_cap_dis[i]*1000/aw
            ax.plot(spec_cap_charge,volts,'g-')
            ax.plot(spec_cap_dis,volts,'g-')
            

    else:
        for i in total_cycles:
            volts = get_column_info('Voltage(V)',i)
            spec_cap = get_column_info(charge_type,i)
            for i in range(0,len(spec_cap)):
                spec_cap[i] = spec_cap[i]*1000/aw
            ax.plot(spec_cap,volts,'g-')
	
    plt.savefig(graph_path)

    print("Finished")

def plot_compare(charge_type): #Plots only the cycles specified by the user input.

    print("")
    print('Type the cycle numbers you want to plot or compare (separate cycle numbers with a comma)')

    compare_cycles = [int(x) for x in input().split(",")]
	
    fig = plt.figure()
    ax = plt.subplot(111)
    plt.title('Voltage vs Specific Capacity')
    plt.ylabel('Voltage (V)')
    plt.xlabel('Specific Capacity (mAh/g)')
    graph_filename = filename + str(' cycle_comparison.png')
    graph_path = path.join(dir,graph_filename)
	
    print("")
    print("...Creating plots.  This may take awhile!...")
    print("")
 
    if charge_type == 'both':
        
        legendColors = ['r','b','g']
        legendColors.append(('#5f317f'))
        legendColors.append(('#000000'))
        cC = 0
        
        for i in compare_cycles:
            volts_charge = get_column_info('Voltage(V)',i,'Discharge_Capacity(Ah)')
            volts_discharge = get_column_info('Voltage(V)',i,'Charge_Capacity(Ah)')
            spec_cap_charge = get_column_info('Charge_Capacity(Ah)',i,'Discharge_Capacity(Ah)')
            spec_cap_dis = get_column_info('Discharge_Capacity(Ah)',i,'Charge_Capacity(Ah)')
                
            label = 'Cycle {}'.format(i)
        
            for i in range(0,len(spec_cap_charge)):
                spec_cap_charge[i] = spec_cap_charge[i]*1000/aw
                spec_cap_dis[i] = spec_cap_dis[i]*1000/aw
            
            if(cC>=len(legendColors)):
                legendColors.append((r.random(),r.random(),r.random()))
                
            ax.plot(spec_cap_charge,volts_charge,label=label,color=legendColors[cC])
            ax.plot(spec_cap_dis,volts_discharge,color=legendColors[cC])
            cC = cC + 1
            
    else:
        
        for i in compare_cycles:
            volts = get_column_info('Voltage(V)',i)
            spec_cap_charge = get_column_info(charge_type,i)
            label = 'Cycle {}'.format(i)
            
            for i in range(0,len(spec_cap_charge)):
                spec_cap_charge[i] = spec_cap_charge[i]*1000/aw

            ax.plot(spec_cap_charge,volts,label=label)
		
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * .85, box.height])
    ax.legend(loc = 'center left', bbox_to_anchor=(1, 0.5))
    plt.savefig(graph_path)
    
    print("")
    print("Finished!")
    print("")

def plot_range(charge_type):  #Plots all cycles within a given range

    print("")
    print('Type the starting and ending cycle of the range you want to graph (separate with a comma)')
	
    cycle_range = input()
    cycle_start = int(cycle_range.split(",")[0])
    cycle_end = int(cycle_range.split(",")[1])
    cycle_range = list(range(cycle_start,cycle_end + 1))

    fig = plt.figure()
    ax = plt.subplot(111)
    plt.suptitle('Voltage vs Specific Capacity',fontsize=20)
    subtitle = str('Cycles ')+str(cycle_start) +str( ' - ')+str(cycle_end)
    plt.title(subtitle,fontsize='smaller')
    plt.ylabel ('Voltage (V)')
    plt.xlabel ('Specific Capacity (mAh/g)')
    graph_filename = filename + str('_cycle_range.png')
    graph_path = path.join(dir,graph_filename)
    	
    print("")
    print("...Creating plots.  This may take awhile!...")
    print("")

    if charge_type == 'both':
        
        legendColors = ['r','b','g']
        legendColors.append(('#5f317f'))
        legendColors.append(('#000000'))
        cC = 0

        for i in cycle_range:
            volts = get_column_info('Voltage(V)',i)
            spec_cap_charge = get_column_info('Charge_Capacity(Ah)',i)
            spec_cap_dis = get_column_info('Discharge_Capacity(Ah)',i)
            label = 'Cycle {}'.format(i)
            
            for i in range(0,len(spec_cap_charge)):
                spec_cap_charge[i] = spec_cap_charge[i]*1000/aw
                spec_cap_dis[i] = spec_cap_dis[i]*1000/aw
        
                       
            if(cC>=len(legendColors)):
                legendColors.append((r.random(),r.random(),r.random()))
                
            ax.plot(spec_cap_charge,volts,label=label,color=legendColors[cC])
            ax.plot(spec_cap_dis,volts,color=legendColors[cC])
            
            cC = cC + 1
    
    else:
        
        for i in cycle_range:
            volts = get_column_info('Voltage(V)',i)
            spec_cap = get_column_info(charge_type,i)
            label = 'Cycle {}'.format(i)
            
            for i in range(0,len(spec_cap)):
                spec_cap[i] = spec_cap[i]*1000/aw
                
            ax.plot(spec_cap,volts,label=label)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * .85, box.height])
    ax.legend(loc = 'center left', bbox_to_anchor=(1, 0.5))
    #ax.legend(loc = 'best')
    plt.savefig(graph_path)
    
    print("")
    print("Finished!")
    print("")
    
###############################################################################



##################### USER INTERFACE/CONTROL PORTION ##########################

a = __name__

if a == '__main__':
    total_cycles, sheet_num, aw, filename, dir, data, stats = get_set_up()

    while a == '__main__':
        
        print("")
        print("YOU CAN CHOOSE FROM THE FOLLOWING:")
        print("")
        print("(1) Plot the voltage profile of a single cycle (or compare multiple individual cycles).")
        print("(2) Plot the voltage profile of a specified range of cycles.")
        print("(3) Plot the voltage profile for all cycles. WARNING: This can take several minutes to plot")
        print("(4) Plot the specific capacity vs cycle.")
        print("(5) Quit")
        print("")
        print("Type 1, 2, 3, or 4 depending on which which function you want to perform. Type 5 to exit")
        print("----------")

        result = input()

        print("")
		
        if result == '1' or result == '2' or result == '3' or result == '4':

            while result == '1' or result == '2' or result == '3' or result == '4':

                if result =='1':
                    print("Choose the type of plot you want to make:")
                    print("(1) Charge voltage profile only")
                    print("(2) Discharge voltage profile only")
                    print("(3) BOTH charge and discharge voltage profile")
                    print("")
                    print("Type 1, 2, or 3 depending on which kind of profile you want")
                    print("----------")
                    profile_answer = input()
                    if profile_answer == '1':
                        plot_compare('Charge_Capacity(Ah)')
                    elif profile_answer == '2':
                          plot_compare('Discharge_Capacity(Ah)')
                    elif profile_answer == '3':
                          plot_compare('both')

                elif result == '2':
                    print("Choose the type of plot you want to make:")
                    print("(1) Charge voltage profile only")
                    print("(2) Discharge voltage profile only")
                    print("(3) BOTH charge and discharge voltage profile")
                    print("")
                    print("Type 1, 2, or 3 depending on which kind of profile you want")
                    print("----------")
                    profile_answer = input()
                    if profile_answer == '1':
                        plot_range('Charge_Capacity(Ah)')
                    elif profile_answer == '2':
                        plot_range('Discharge_Capacity(Ah)')
                    elif profile_answer == '3':
                        plot_range('both')

                elif result == '3':
                    print("Choose the type of plot you want to make:")
                    print("(1) Charge voltage profile only")
                    print("(2) Discharge voltage profile only")
                    print("(3) BOTH charge and discharge voltage profile")
                    print("")
                    print("Type 1, 2, or 3 depending on which kind of profile you want")
                    print("----------")
                    profile_answer = input()
                    if profile_answer == '1':
                        plot_all('Charge_Capacity(Ah)')
                    elif profile_answer == '2':
                        plot_all('Discharge_Capacity(Ah)')
                    elif profile_answer == '3':
                        plot_all('both')
                        
                elif result == '4':
                    skip_cycles = skip_cycle()
                    plot_capacity()
                    
                else:
                    result = '0'

                print("Would you like to preform a different plotting function for this file? [y/n]")

                answer = input()
                answer_low = answer.lower()

                if answer_low == 'y' or answer_low == 'yes':
                    result = '0'

                if answer_low == 'n' or answer_low =='no':
                    result = '0'
                    a = "none"

        elif result == '5':
            result = '0'
            a = "none"
        else:
            print("That wasn't a valid choice, please try again.")
            print("")
            result = 'none'

################################ END ##########################################
