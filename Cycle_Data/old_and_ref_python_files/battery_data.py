#Usr this python code to open and read the excel files from Arbin and graph the data#

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import os as os
from os import path
import shutil as shutil

##########################

#Opens the excel file we want to graph:
print('Type the name of the file you want to import. (Cap sensitive)')
filename=input()  #Type in the exact file name of the spreadsheet Arbin exports without the file extension

if os.path.isfile(filename+str(".xlsx")) is True:
	data_file =(filename+'.xlsx') #adds the correct file extension needed for the variable data
else:
	data_file =(filename+'.xls')

#Collects active weight information from a manual user input:
print('Enter the active weight of your sample (in grams)')
aw=float(input())

data=pd.read_excel(data_file,sheetname=-1) #Reads only the  Statistics tab of the Arbin excel file

#Assigns each individual column as its own variable#
cycle=np.array(data['Cycle_Index'])
current=np.array(data['Current(A)'])
volt=np.array(data['Voltage(V)'])
charge_cap=np.array(data['Charge_Capacity(Ah)'])
dcharge_cap=np.array(data['Discharge_Capacity(Ah)'])

#Convert and calculate new data:
mAh_charge = (charge_cap)*(1000) #Convert charge capacity from aH to mAh
mAh_discharge = (dcharge_cap)*(1000) #Converts the discharge capacity from aH to mAh
spec_cap_charge = (mAh_charge)/(aw) #Divides the capacity(mAh) by the active weight (aw)
spec_cap_discharge = (mAh_discharge)/(aw)
ce = ((spec_cap_charge/spec_cap_discharge)*100)   #Ratio of Charge Specific Capacity to Discharge Specific Capacity

#MAKES A NEW FOLDER WITH THE SAME NAME AS THE FILE (USER INPUT FILENAME)#
dir=(filename)
if os.path.exists(dir):
	shutil.rmtree(dir)
os.makedirs(dir)

#Give titles and saved-path locations for any saved graphs and/or excel files:
graphtitle_speccap = filename+str('_specific_capacity_graph.png')
graph_path_speccap = path.join(filename,graphtitle_speccap)

graphtitle_charge = filename+str('_charge_capacity.png')
graph_path_charge = path.join(filename,graphtitle_charge)

graphtitle_discharge = filename+str('_discharge_capacity.png')
graph_path_discharge = path.join(filename,graphtitle_discharge)

extitle = filename+str('_data.xlsx')   
path = path.join(filename,extitle)


def plot_data():

        #Plot of specific capacity vs cycle#
        plt.figure(1)
        plt.title('Specific Capacity vs Cycle')
        plt.plot(cycle,spec_cap_charge,'go',label='Charge')
        plt.plot(cycle,spec_cap_discharge,'ro',label='Discharge')
        plt.ylabel('Specific Capacity (mAh/g)')
        plt.xlabel('Cycle Number')
        plt.legend(loc='upper right',shadow=True, fancybox=True, numpoints=1)
        plt.savefig(graph_path_speccap)

        #Plot of Charge Specific Capacity & Coulombic Efficiency vs Cycle Number:
        fig2, ax1 = plt.subplots()
        ax1.set_title('Charge Capacity vs Cycle')
        color = 'green'
        ax1.set_xlabel('Cycle Number')
        ax1.set_ylabel('Specific Capacity (mAh/g)',color=color)
        ax1.plot(cycle,spec_cap_charge,'g^')
        ax1.tick_params(axis='y',labelcolor=color)

                #Initiate the 2nd Y axis:
        ax2 = ax1.twinx()
        color = 'blue'
        ax2.set_ylabel('Coulombic Efficiency')
        ax2.plot(cycle,ce,'bo')
        ax2.tick_params(axis='y', labelcolor=color)
        fig2.tight_layout()
        plt.savefig(graph_path_charge)

        #Plot of Discharge Specific Capacity & Coulombic Efficiency vs Cycle Number:
        fig3, ax3 = plt.subplots()
        ax3.set_title('Discharge Capacity vs Cycle')
        color = 'red'
        ax3.set_xlabel('Cycle Number')
        ax3.set_ylabel('Specific Capacity (mAh/g)',color=color)
        ax3.plot(cycle,spec_cap_discharge,'r^')
        ax3.tick_params(axis='y',labelcolor=color)

                #Initiate the 2nd y-axis:
        ax4 = ax3.twinx()
        color = 'blue'
        ax4.set_ylabel('Coulombic Efficiency')
        ax4.plot(cycle,ce,'bo')
        ax4.tick_params(axis='y', labelcolor=color)
        fig3.tight_layout()
        plt.savefig(graph_path_discharge)	
                


        #SAVING THE SPECIFIC CAPACITY, COULOMBIC EFF, AND CYCLE TO EXCEL#
        df=pd.DataFrame({'Cycle':cycle, 'Charge Specific Capacity':spec_cap_charge, 'Discharge Specific Capacity':spec_cap_discharge, 'Coulombic Efficiency':ce},columns=['Cycle','Charge Specific Capacity','Discharge Specific Capacity','Coulombic Efficiency'])
        writer = pd.ExcelWriter((path), engine='xlsxwriter')
        df.to_excel(writer, sheet_name = filename)
        writer.save()

        print ("Data Plotting Finished")

plot_data()





