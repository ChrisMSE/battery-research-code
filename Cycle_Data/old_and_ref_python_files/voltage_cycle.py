import sys 
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import os as os
from os import path
from textwrap import wrap
import shutil as shutil

######################### PRELIMINARY/HOUSE KEEPING ###########################
def get_set_up():

	print("Type the name of the file you want to import (Caps sensitive)")
	filename = input()	
	filename = filename.split(".")[0]
	print("")
	print('Type the active weight of your sample (in grams):')
	aw = float(input())
	print("")
	if os.path.isfile(filename+str(".xlsx")) is True:
		data_file =(filename+'.xlsx') #adds the correct file extension needed 
							    #for the variable data
	else:
		data_file =(filename+'.xls')
	print("...Loading the spreadsheet - This might take awhile!...")
	data = pd.ExcelFile(data_file) #Opens the excel file

	#Creates a folder to save graphs and excel spreadsheets:

	dir = (filename + str(" Voltage Data"))
	if os.path.exists(dir):
		shutil.rmtree(dir)
	os.makedirs(dir)

	#Open the stats worksheet to determine the total number of cycles:

	stats = pd.read_excel(data,sheetname = -1)
	total_cycles = np.array(stats['Cycle_Index'])

	#Gets a list of sheet names to determine how many sheets contian data:

	sheets = (data.sheet_names)
	sheet_num = len(data.sheet_names) - 1

	return total_cycles, sheet_num, aw, filename, dir, data

###############################################################################


############### GRABS CYCLE DATA FROM THE ARBIN SPREADSHEET ###################

def get_column_info(column_name,index):
	currIndex = 0
	retVal = []
	for i in range(1,sheet_num):
		if(currIndex>index):break
		
		ws = pd.read_excel(data,sheetname=i)
		volt = np.array(ws[column_name])
		index_list = np.array(ws["Cycle_Index"])
		n = 0
		while(currIndex <= index and n < len(index_list)):
			if(index_list[n] == index):
				retVal.append(volt[n])
			currIndex = index_list[n]
			n = n + 1
	return retVal

          # All of the functions below rely on and use this
          # function to return the needed data  #

###############################################################################


################### 3 DIFFERENT POSSIBLE PLOTTING FUNCTONS ####################

def plot_all():  #Plots all of the cycles recorded on the stats worksheet
	fig = plt.figure()
	ax = plt.subplot(111)
	plt.title ('Voltage vs Specific Capacity')
	plt.ylabel ('Voltage (V)')
	plt.xlabel ('Specific Capacity (mAh/g)')
	graph_filename = filename + str('_all_cycles.png')
	graph_path = path.join(dir,graph_filename)

	print("")
	print("Plotting all cycles can take a long time. Please be patient!")
	print("")

	for i in total_cycles:
		volts = get_column_info('Voltage(V)',i)
		spec_cap = get_column_info('Charge_Capacity(Ah)',i)
		for i in range(0,len(spec_cap)):
			spec_cap[i] = spec_cap[i]*1000/aw
		ax.plot(spec_cap,volts,'g-')
	
	plt.savefig(graph_path)

	print("Finished")

def plot_compare(): #Plots only the cycles specified by the user input.

	print("")
	print('Type the cycle numbers you want to compare (separate cycle numbers with a comma)')

	compare_cycles = [int(x) for x in input().split(",")]
	
	fig = plt.figure()
	ax = plt.subplot(111)
	plt.title('Voltage vs Specific Capacity')
	plt.ylabel('Voltage (V)')
	plt.xlabel('Specific Capacity (mAh/g)')
	graph_filename = filename + str(' cycle_comparison.png')
	graph_path = path.join(dir,graph_filename)
	
	print("")
	print("...Creating plots.  This may take awhile!...")
	print("")
	
	for i in compare_cycles:
		volts = get_column_info('Voltage(V)',i)
		spec_cap = get_column_info('Charge_Capacity(Ah)',i)
		label = 'Cycle {}'.format(i)
		for i in range(0,len(spec_cap)):
			spec_cap[i] = spec_cap[i]*1000/aw

		ax.plot(spec_cap,volts,label=label)
		

	ax.legend(loc = 'best')
	plt.savefig(graph_path)
		
	print("Finished!")

def plot_range():  #Plots all cycles within a given range

	print("")
	print('Type the starting and ending cycle of the range you want to graph (separate with a comma)')
	
	cycle_range = input()
	cycle_start = int(cycle_range.split(",")[0])
	cycle_end = int(cycle_range.split(",")[1])
	cycle_range = list(range(cycle_start,cycle_end + 1))

	fig = plt.figure()
	ax = plt.subplot(111)
	plt.suptitle('Voltage vs Specific Capacity',fontsize=20)
	subtitle = str('Cycles ')+str(cycle_start) +str( ' - ')+str(cycle_end)
	plt.title(subtitle)
	plt.ylabel ('Voltage (V)')
	plt.xlabel ('Specific Capacity (mAh/g)')
	graph_filename = filename + str('_cycle_range.png')
	graph_path = path.join(dir,graph_filename)
	
	print("")
	print("...Creating plots.  This may take awhile!...")
	print("")

	for i in cycle_range:
		volts = get_column_info('Voltage(V)',i)
		spec_cap = get_column_info('Charge_Capacity(Ah)',i)
		for i in range(0,len(spec_cap)):
			spec_cap[i] = spec_cap[i]*1000/aw
		ax.plot(spec_cap,volts,'g-')

	plt.savefig(graph_path)

	print("Finished!")

###############################################################################

##################### USER INTERFACE/CONTROL PORTION ##########################

a = __name__

if a == '__main__':
    total_cycles, sheet_num, aw, filename, dir, data = get_set_up()

    while a == '__main__':
        print("")
        print("You can choose from the following: (1) Plot a single or multiple individual cycles. (2) Plot a specific range of cycles. (3) Plot all cycles.")
        print("")
        print("Type 1, 2, or 3 depending on which which function you want to perform.")

        result = input()

        print("")
		
        if result == '1' or result == '2' or result == '3':

            while result == '1' or result == '2' or result == '3':

                if result =='1':
                    plot_compare()

                elif result == '2':
                    plot_range()

                elif result == '3':
                    plot_all()

                else:
                    result = '0'

                print("Would you like to preform a different plotting function for this file? [y/n]")

                answer = input()
                answer_low = answer.lower()

                if answer_low == 'y' or answer_low == 'yes':
                    result = '0'

                if answer_low == 'n' or answer_low =='no':
                    result = '0'
                    a = "none"

        else:
            print("That wasn't a valid choice, please try again.")
            print("")
            result = 'none'

################################ END ##########################################
