import numpy as np
import pandas as pd
from pandas import ExcelFile
from pandas import ExcelWriter
import scipy as sp
import matplotlib.pyplot as plt

'''

Module that will contain functions that each perform some type of
quantitative analysis on the data collected from the arbin.

As of now, this will only work with Battery_Data_Module.py

'''
##
##capacity=[1.02,1.21,1.38,1.52]
##voltage=[-3.4,-2.89,-2.12,-1.45]


def diff_cap(capacity,voltage):
    
    diff_cap = []
    diff_volt = []

    for i in range(len(capacity)-1):
        diff = ((capacity[i+1]) - (capacity[i]))
        diff_cap.append(diff)
        
    for i in range(len(voltage)-1):
        diff = ((voltage[i+1]) - (voltage[i]))
        diff_volt.append(diff)

    diff_cap = np.array(diff_cap)
    diff_volt = np.array(diff_volt)
    dc_dv = np.divide(diff_cap , diff_volt)

    fig = plt.figure()
    plot = plt.subplot(111)
    plt.title('Differential Capacity')
    plt.ylabel('dQ/dV')
    plt.xlabel('Voltage (V)')
    plot.plot(voltage[1:],dc_dv)
    plt.show()

    return plt.show(),plot

##def voltage_hysteresis(charge_capacity,charge_voltage,discharge_capacity,discharge_voltage):
##    pass
##
##def capacity_change():
##    pass
##
##diff_cap(capacity, voltage)


    


        

