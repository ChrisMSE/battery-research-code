#Usr this python code to open and read the excel files from Arbin and graph the data#

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import os as os
from os import path
from textwrap import wrap
import shutil as shutil
import csv

xbase = 0
xaxlim = 500
ybase = 0
yaxlim = 5
maxtickspreadX = 50
mintickspreadX = 10
maxtickspreadY = 1
mintickspreadY = 0.25	
yLabel = ""
xLabel = ""
title = "Voltage(V) vs. Specific Capacity(mAh)"

cycle = 9 

print('Type the name of the file you want to import. (Cap sensitive)')
filename=input()  #Type in the exact file name of the spreadsheet Arbin exports
                  #without the file extension

print('Enter the active weight of your sample (in grams)')
aw = float(input())

#Opens the excel file we want to graph:
filename = filename.split(".")[0]

if os.path.isfile(filename+str(".xlsx")) is True:
	data_file =(filename+'.xlsx') #adds the correct file extension needed 
				      #for the variable data

else:
	data_file =(filename+'.xls')

data = pd.ExcelFile(data_file) #Opens the excel file
sheets = (data.sheet_names)
sheet_num = len(data.sheet_names) - 1

#Returnss relevant information for the specified column
def get_column_info(column_name,index):
	currIndex = 0
	retVal = []
	for i in range(1,sheet_num):
		if(currIndex>index):break
		
		ws = pd.read_excel(data,sheetname=i)
		volt = np.array(ws[column_name])
		index_list = np.array(ws["Cycle_Index"])
		n = 0
		while(currIndex <= index and n < len(index_list)):
			if(index_list[n] == index):
				retVal.append(volt[n])
			currIndex = index_list[n]
			n = n + 1
	return retVal

#Example of getting volts and specific capacity columns for the specified cycle
yLabel = "Voltage(V)"
xLabel = "Specific Capacity(mAh)"
volts = get_column_info(yLabel,cycle)
specap = get_column_info("Charge_Capacity(Ah)",cycle)
for i in range(0,len(specap)):
	specap[i] = specap[i]*1000/aw

#Figure and graph beautification
fig = plt.figure()
ax = fig.add_subplot(1,1,1)

major_ticksx = np.arange(0,xaxlim,maxtickspreadX)
minor_ticksx = np.arange(0,xaxlim,mintickspreadX)
major_ticksy = np.arange(0,yaxlim,maxtickspreadY)
minor_ticksy = np.arange(0,yaxlim,mintickspreadY)

ax.set_xticks(major_ticksx)
ax.set_xticks(minor_ticksx, minor=True)
ax.set_yticks(major_ticksy)
ax.set_yticks(minor_ticksy, minor=True)

ax.grid(which='both')

ax.grid(which='minor', alpha=0.5)
ax.grid(which='major', alpha=1)

#Set axis limits
plt.xlim(xbase,xaxlim)
plt.ylim(ybase,yaxlim)

#Label axis
plt.ylabel(yLabel)
plt.xlabel(xLabel)

#Graph title
ax.set_title("\n".join(wrap(title,60)))

#Set style 
plt.style.use('ggplot')

#Plot the data
plt.plot(specap,volts)
#Important to note that .show() is not required but if you do a save you
#need to do it below the show otherwise the show will show a blank fig
plt.show()
#fig.savefig(#FILENAME#)
