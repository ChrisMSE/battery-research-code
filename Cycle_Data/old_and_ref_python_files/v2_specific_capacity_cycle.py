import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import os as os
from os import path
import shutil as shutil


def get_started():

	##Opens the excel file we want to graph:

    print('Type the name of the file you want to import. (Cap sensitive)')
    filename=input()
    filename = filename.split(".")[0]

    if os.path.isfile(filename+str(".xlsx")) is True:  #adds the correct file extension needed for the variable data
        data_file =(filename+'.xlsx') 
    else:
        data_file =(filename+'.xls')
        
	##Collects active weight information from a manual user input:

    print("")
    print('Enter the active weight of your sample (in grams)')
    aw=float(input())

    data = pd.read_excel(data_file,sheetname=-1) #Opens the last worksheet of the imported excel file

	##Makes a new folder with the same name as the imported file:

    dir=(filename + str(" Capacity Data"))
    if os.path.exists(dir):
        shutil.rmtree(dir)
    os.makedirs(dir)

    return filename, dir, data

##Creates file names for graphs, excel sheets, and location to save the files:
graphtitle_speccap = filename+str('_specific_capacity_graph.png')
graph_path_speccap = path.join(dir,graphtitle_speccap)
graphtitle_charge = filename+str('_charge_capacity.png')
graph_path_charge = path.join(dir,graphtitle_charge)
graphtitle_discharge = filename+str('_discharge_capacity.png')
graph_path_discharge = path.join(dir,graphtitle_discharge)
extitle = filename+str('_data.xlsx')
path = path.join(dir,extitle)

############################ SKIPPING CYCLES ##################################

print('Do you want to skip any cycles before graphing the data? (y/n)')

answer = input()	

if answer == 'n' or answer == 'N':
	skip_cycles=[]

elif answer == 'y' or answer == 'Y':
	print('Enter the cycle number(s) (separate with a comma if more than one)')
	skip_cycles = [int(x) for x in input().split(",")]

else:
	print('Please select yes or no (y/n)')

###############################################################################

####### FUNCTION THAT GRABS THE NEEDED DATA FROM THE ARBIN EXCEL FILE #########

def grab_data(column_name):

	retVal = []

	if not skip_cycles:
		values = data[column_name]
		retVal.append(values)

	else:
		values = data[column_name]
		for i in skip_cycles:
			del values[i - 1]
			new_values = values
			retVal.append(new_values)
	return retVal

###############################################################################


############ CREATING NEEDED VARIABLES USING grab_data FUNCTION ###############

cycle = np.array(grab_data('Cycle_Index'))
charge_capacity = np.array(grab_data('Charge_Capacity(Ah)'))
discharge_capacity = np.array(grab_data('Discharge_Capacity(Ah)'))
charge_specific_capacity = (charge_capacity) * (1000) / aw
discharge_specific_capacity = (discharge_capacity) * ( 1000) / aw
col_eff = ((charge_specific_capacity)/(discharge_specific_capacity)) * 100

# FLATTEN THE ARRAYS FOR GRAPHING #
cycle_flat = cycle.flatten()
charge_specific_capacity_flat = charge_specific_capacity.flatten()
discharge_specific_capacity_flat = discharge_specific_capacity.flatten()
col_eff_flat = col_eff.flatten()

###############################################################################

######################## PLOTTING AND SAVING GRAPHS ###########################

#Specific Capacity vs Cycle#
fig = plt.figure()
ax = plt.subplot(111)
ax.plot(cycle_flat,charge_specific_capacity_flat,'go',label='Charge')
ax.plot(cycle_flat,discharge_specific_capacity_flat,'ro',label='Discharge')
plt.title('Specific Capacity vs Cycle')
plt.ylabel ('Specific Capacity (mAh/g)')
plt.xlabel('Cycle Number')
plt.legend(loc='upper right',shadow=True, fancybox=True, numpoints=1)
plt.savefig(graph_path_speccap)

#Charge Specific Capacity & Coulombic Eff#
fig2, ax1 = plt.subplots()
ax1.set_title('Charge Capacity vs Cycle')
ax1.set_xlabel('Cycle Number')
ax1.set_ylabel('Specific Capacity (mAh/g)',color='green')
ax1.plot(cycle_flat,charge_specific_capacity_flat,'g^')
ax1.tick_params(axis='y',labelcolor='green')

                #Initiate the 2nd Y axis:
ax2 = ax1.twinx()
ax2.set_ylabel('Coulombic Efficiency (%)',color='blue')
ax2.plot(cycle_flat,col_eff_flat,'bo')
ax2.tick_params(axis='y', labelcolor='blue')
fig2.tight_layout()
plt.savefig(graph_path_charge)

#Discharge Specific Capacity & Coulombic Eff#
fig3, ax1 = plt.subplots()
ax1.set_title('Discharge Capacity vs Cycle')
ax1.set_xlabel('Cycle Number')
ax1.set_ylabel('Specific Capacity (mAh/g)',color='red')
ax1.plot(cycle_flat,discharge_specific_capacity_flat,'r^')
ax1.tick_params(axis='y',labelcolor='red')

                #Initiate the 2nd Y axis:
ax2 = ax1.twinx()
ax2.set_ylabel('Coulombic Efficiency (%)',color='blue')
ax2.plot(cycle_flat,col_eff_flat,'bo')
ax2.tick_params(axis='y', labelcolor='blue')
fig2.tight_layout()
plt.savefig(graph_path_discharge)

###############################################################################
        

########## CREATING AND SAVING EXCEL SPREADSHEET WITH DATA SUMMARY ############

#Creates and saves an excel file that summarizes the specific capacity (mAh/g) and cycles:
df=pd.DataFrame({'Cycle':cycle_flat, 'Charge Specific Capacity':charge_specific_capacity_flat, 'Discharge Specific Capacity':discharge_specific_capacity_flat, 'Coulombic Efficiency':col_eff_flat},columns=['Cycle','Charge Specific Capacity','Discharge Specific Capacity','Coulombic Efficiency'])
writer = pd.ExcelWriter((path), engine='xlsxwriter')
df.to_excel(writer, sheet_name = filename)
writer.save()

###############################################################################

print ("Finished!")









####  OLD STUFF #####

# GRAB DATA FUNCTION BEFORE ADDING THE SKIP CYCLE FEATURE #
#def grab_data(column_name):
#       retVal=[]
#
#       if not  skip_cycles:
#               values = np.array(data[column_name])
#               retVal.append(values)
#               array = np.array(retVal)
#               array_flat = array.flatten()
#       else:
#               values = np.array(data[column_name])
#
#               for i in skip_cycles:
#                       new_values = values.remove(i)
#                       retVal.append(new_values)
#                       array = np.array(retVal)
#                       array_flat = array.flaten()
#       
#       return array_flat 




