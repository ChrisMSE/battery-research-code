import sys 
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import os as os
from os import path
from textwrap import wrap
import shutil as shutil
import random as r

######################### PRELIMINARY/HOUSE KEEPING ###########################
def get_set_up():

    print("Type the name of the file you want to import (Caps sensitive)")
    filename = input()	
    filename = filename.split(".")[0]
    print("")
    print('Type the active weight of your sample (in milligrams):')
    aw = float(input())
    print("")
    if os.path.isfile(filename+str(".xlsx")) is True:
        data_file =(filename+'.xlsx') #adds the correct file extension needed 
							    #for the variable data
    else:
        data_file =(filename+'.xls')
        print("...Loading the spreadsheet - This might take awhile!...")
        data = pd.ExcelFile(data_file) #Opens the excel file

	#Creates a folder to save graphs and excel spreadsheets:

    dir = (filename + str(" Voltage Data"))
    if os.path.exists(dir):
        shutil.rmtree(dir)
    os.makedirs(dir)

	#Open the stats worksheet to determine the total number of cycles:

    stats = pd.read_excel(data,sheetname = -1)
    total_cycles = np.array(stats['Cycle_Index'])

	#Gets a list of sheet names to determine how many sheets contian data:

    sheets = (data.sheet_names)
    sheet_num = len(data.sheet_names) - 1

    return total_cycles, sheet_num, aw, filename, dir, data, stats

###############################################################################


############### GRABS CYCLE DATA FROM THE ARBIN SPREADSHEET ###################

def get_column_info(column_name,index):
    currIndex = 0
    retVal = []
    for i in range(1,sheet_num):
        if(currIndex>index):break
	
        ws = pd.read_excel(data,sheetname=i)
        volt = np.array(ws[column_name])
        index_list = np.array(ws["Cycle_Index"])
        n = 0
        
        while(currIndex <= index and n < len(index_list)):
            if(index_list[n] == index):
                retVal.append(volt[n])
            currIndex = index_list[n]
            n = n + 1
            
    return retVal

    # All of the functions below rely on and use this
    # function to return the needed data  #

###############################################################################

def skip_cycle():
    
    print("")
    print("Do you want to omit any cycles before graphing the data? [y/n]")
    answer = input()
    answer_low = answer.lower()
    
    if answer_low == 'n' or answer_low == 'no':
        skip_cycles = []
        
    elif answer_low == 'y' or answer_low == 'yes':
        print("")
        print("Enter the cycle number(s) you want to omit (Separate each with a comma if there is more than one).")
        skip_cycles = [int(x) for x in input().split(",")]
        
    else:
        print("That wasn't a valid choice. Please select yes or no [y/n]")
        
    return skip_cycles

def get_capacity(column_name):

    skip_cycle()
    
    retVal = []

    if not skip_cycles:
        values = stats[column_name]
        retVal.append(values)
        
    else:
        values = stats[column_name]
        for i in skip_cycles:
            del values[i - 1]
            new_values = values
        retVal.append(new_values)
        
    return retVal

################### 4 DIFFERENT POSSIBLE PLOTTING FUNCTONS ####################

def plot_capacity():

    #Define the needed variables#
    cycle = np.array(get_capacity('Cycle_Index'))
    charge_capacity = np.array(get_capacity('Charge_Capacity(Ah)'))
    discharge_capacity = np.array(get_capacity('Discharge_Capacity(Ah)'))
    charge_specific_capacity = (charge_capacity) * (100/aw)
    discharge_specific_capacity = (discharge_capacity) * (100/aw)
    col_eff = ((charge_specific_capacity)/(discharge_specific_capacity)) * 100

   #Flatten the arrays for graphing#
    cycle_flat = cycle.flatten()
    charge_specific_capacity_flat = charge_specific_capacity.flaten()
    discharge_specific_capacity_flat = discharge_sepcific_capacity.flaten()
    col_eff_flat = col_eff.flatten()

    graphtitle_speccap = filename+str('_specific_capacity_graph.png')
    graph_path_speccap = path.join(dir,graphtitle_speccap)
    graphtitle_charge = filename+str('_charge_capacity.png')
    graph_path_charge = path.join(dir,graphtitle_charge)
    graphtitle_discharge = filename+str('_discharge_capacity.png')
    graph_path_discharge = path.join(dir,graphtitle_discharge)
    extitle = filename+str('_data.xlsx')
    path = path.join(dir,extitle)

    #Specific Capacity vs Cycle#
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.plot(cycle_flat,charge_specific_capacity_flat,'go',label='Charge')
    ax.plot(cycle_flat,discharge_specific_capacity_flat,'ro',label='Discharge')
    plt.title('Specific Capacity vs Cycle')
    plt.ylabel ('Specific Capacity (mAh/g)')
    plt.xlabel('Cycle Number')
    plt.legend(loc='upper right',shadow=True, fancybox=True, numpoints=1)
    plt.savefig(graph_path_speccap)

#Charge Specific Capacity & Coulombic Eff#
    fig2, ax1 = plt.subplots()
    ax1.set_title('Charge Capacity vs Cycle')
    ax1.set_xlabel('Cycle Number')
    ax1.set_ylabel('Specific Capacity (mAh/g)',color='green')
    ax1.plot(cycle_flat,charge_specific_capacity_flat,'g^')
    ax1.tick_params(axis='y',labelcolor='green')

                #Initiate the 2nd Y axis:
    ax2 = ax1.twinx()
    ax2.set_ylabel('Coulombic Efficiency (%)',color='blue')
    ax2.plot(cycle_flat,col_eff_flat,'bo')
    ax2.tick_params(axis='y', labelcolor='blue')
    fig2.tight_layout()
    plt.savefig(graph_path_charge)

#Discharge Specific Capacity & Coulombic Eff#
    fig3, ax1 = plt.subplots()
    ax1.set_title('Discharge Capacity vs Cycle')
    ax1.set_xlabel('Cycle Number')
    ax1.set_ylabel('Specific Capacity (mAh/g)',color='red')
    ax1.plot(cycle_flat,discharge_specific_capacity_flat,'r^')
    ax1.tick_params(axis='y',labelcolor='red')

                #Initiate the 2nd Y axis:
    ax2 = ax1.twinx()
    ax2.set_ylabel('Coulombic Efficiency (%)',color='blue')
    ax2.plot(cycle_flat,col_eff_flat,'bo')
    ax2.tick_params(axis='y', labelcolor='blue')
    fig2.tight_layout()
    plt.savefig(graph_path_discharge)
    
    #Saves the data to a new excel file:
    df=pd.DataFrame({'Cycle':cycle_flat, 'Charge Specific Capacity':charge_specific_capacity_flat, 'Discharge Specific Capacity':discharge_specific_capacity_flat, 'Coulombic Efficiency':col_eff_flat},columns=['Cycle','Charge Specific Capacity','Discharge Specific Capacity','Coulombic Efficiency'])
    writer = pd.ExcelWriter((path), engine='xlsxwriter')
    df.to_excel(writer, sheet_name = filename)
    writer.save()

    print("Finished!")


def plot_all():  #Plots all of the cycles recorded on the stats worksheet
    fig = plt.figure()
    ax = plt.subplot(111)
    plt.title ('Voltage vs Specific Capacity')
    plt.ylabel ('Voltage (V)')
    plt.xlabel ('Specific Capacity (mAh/g)')
    graph_filename = filename + str('_all_cycles.png')
    graph_path = path.join(dir,graph_filename)

    print("")
    print("Plotting all cycles can take a long time. Please be patient!")
    print("")

    for i in total_cycles:
        volts = get_column_info('Voltage(V)',i)
        spec_cap = get_column_info('Charge_Capacity(Ah)',i)
        for i in range(0,len(spec_cap)):
            spec_cap[i] = spec_cap[i]*1000/aw
        ax.plot(spec_cap,volts,'g-')
	
    plt.savefig(graph_path)

    print("Finished")

def plot_compare(): #Plots only the cycles specified by the user input.

    print("")
    print('Type the cycle numbers you want to compare (separate cycle numbers with a comma)')

    compare_cycles = [int(x) for x in input().split(",")]
	
    fig = plt.figure()
    ax = plt.subplot(111)
    plt.title('Voltage vs Specific Capacity')
    plt.ylabel('Voltage (V)')
    plt.xlabel('Specific Capacity (mAh/mg)')
    graph_filename = filename + str(' cycle_comparison.png')
    graph_path = path.join(dir,graph_filename)
	
    print("")
    print("...Creating plots.  This may take awhile!...")
    print("")
    
    legendColors = ['r','b','g']
    legendColors.append(('#5f317f'))
    legendColors.append(('#000000'))
    cC = 0
    for i in compare_cycles:
        volts = get_column_info('Voltage(V)',i)
        spec_cap = get_column_info('Charge_Capacity(Ah)',i)
        spec_cap_dis = get_column_info('Discharge_Capacity(Ah)',i)
        #cap_combined = spec_cap + spec_cap_dis
        
        label = 'Cycle {} Charge'.format(i)
        label2 = 'Cycle {} Discharge'.format(i)
        for i in range(0,len(spec_cap)):
            spec_cap[i] = spec_cap[i]*1000000/aw
            spec_cap_dis[i] = spec_cap_dis[i]*1000000/aw
            #combined[i] = combined[i]*1000000/aw
        if(cC>=len(legendColors)):
                        legendColors.append((r.random(),r.random(),r.random()))
        ax.plot(spec_cap,volts,label=label,color=legendColors[cC])
        ax.plot(spec_cap_dis,volts,label=label2,color=legendColors[cC])
        cC = cC + 1
        #ax.plot(combined,volts,label=label)
		

    ax.legend(loc = 'best')
    plt.savefig(graph_path)
		
    print("Finished!")

def plot_range():  #Plots all cycles within a given range

    print("")
    print('Type the starting and ending cycle of the range you want to graph (separate with a comma)')
	
    cycle_range = input()
    cycle_start = int(cycle_range.split(",")[0])
    cycle_end = int(cycle_range.split(",")[1])
    cycle_range = list(range(cycle_start,cycle_end + 1))

    fig = plt.figure()
    ax = plt.subplot(111)
    plt.suptitle('Voltage vs Specific Capacity',fontsize=20)
    subtitle = str('Cycles ')+str(cycle_start) +str( ' - ')+str(cycle_end)
    plt.title(subtitle)
    plt.ylabel ('Voltage (V)')
    plt.xlabel ('Specific Capacity (mAh/g)')
    graph_filename = filename + str('_cycle_range.png')
    graph_path = path.join(dir,graph_filename)
	
    print("")
    print("...Creating plots.  This may take awhile!...")
    print("")

    for i in cycle_range:
        volts = get_column_info('Voltage(V)',i)
        spec_cap = get_column_info('Charge_Capacity(Ah)',i)
        for i in range(0,len(spec_cap)):
            spec_cap[i] = spec_cap[i]*1000/aw
        ax.plot(spec_cap,volts,'g-')

    plt.savefig(graph_path)

    print("Finished!")

###############################################################################

##################### USER INTERFACE/CONTROL PORTION ##########################

a = __name__

if a == '__main__':
    total_cycles, sheet_num, aw, filename, dir, data, stats = get_set_up()

    while a == '__main__':
        print("")
        print("You can choose from the following: (1) Plot a single or multiple individual cycles. (2) Plot a specific range of cycles. (3) Plot all cycles. (4) Plot the specific capacity.")
        print("")
        print("Type 1, 2, 3, or 4 depending on which which function you want to perform.")

        result = input()

        print("")
		
        if result == '1' or result == '2' or result == '3' or result == '4':

            while result == '1' or result == '2' or result == '3' or result == '4':

                if result =='1':
                    plot_compare()

                elif result == '2':
                    plot_range()

                elif result == '3':
                    plot_all()

                elif result == '4':
                    plot_capacity()
                    
                else:
                    result = '0'

                print("Would you like to preform a different plotting function for this file? [y/n]")

                answer = input()
                answer_low = answer.lower()

                if answer_low == 'y' or answer_low == 'yes':
                    result = '0'

                if answer_low == 'n' or answer_low =='no':
                    result = '0'
                    a = "none"

        else:
            print("That wasn't a valid choice, please try again.")
            print("")
            result = 'none'

################################ END ##########################################
