'''
Raw Data Voltage vs Current Capacity Visualization Software

Filename:               RawConversiontoPNGandXLS.py
Author:                 Steven Letourneau (steveletourneau@boisestate.edu)
'''
Pdate = '2014 Dec 19'
Pversion = '1.5'
'''
Program Description:    The progam will take text files from Cycler and create PNG files to visualize
                        the V vs A curves.  It creates one combined, one efficiency and single graph for
                        each cycle. It will also output an excel file with the raw data in two column format.
                        
Operating Insturction:  First, make sure SciPy is installed, if double click does not work on the RawConversiontoPNG.py file
                        follow the step below. Once running, the first propmpt will allow you to select the file to convert.  
                        Next, in the command prompt window Enter the number of cycles. Enter the mass in grams of the active 
                        material and press Enter.  The program will generate the the pictures with the text filename.  The program
                        will also produce a single excel sheet with the data output for analysis.

SciPy Link:             http://winpython.sourceforge.net/

If double click 
isn't working:          Right click on the .py file, select Open With.  Browse to the SciPy install directory and click OK.
                        This should work unless there are other python installations on the computer.  If so you may need to 
                        manually add the SciPy install to the computers Path variable.  (Use google, very easy to follow).
                                               

'''
####################################
##  Import libraries ##
try:
    from matplotlib.pyplot import *
    from pylab import *
    from Tkinter import Tk
    from tkFileDialog import askopenfilename
    import os
    import xlwt
except:
    print ("Ooops!  There seems to be some packages missing\nPlease open the python console and attempt to import matplolib, pylab and Tkinter")
####################################
## SETTING PARAMETERS ##
begin_of_data = 5 # IF THE LINE CHANGES FOR THE HEADER OF THE FILE, THIS NEEDS TO BE CHANGED!!!!!!!!!


####################################
## GLOBAL VARIABLES ##               FOR FUTURE RELEASES
filepath = ''
num_of_cycles = ''
workingdir = ''
data_filename = ''

#####################
## Import text file 

# Open window to select a file
Tk().withdraw()
filepath = askopenfilename()
print 'File selected: \n' + filepath +'\n'

# Back to console
num_of_cycles = float(raw_input('Enter the number of cycles :'))
mass_active_mat = float(raw_input('Enter the mass (g) of active material :'))
images = 'n'
images = raw_input('Do you want images created? y or n :')
f = open(filepath, 'r')
whole_file = f.readlines()
f.close

# Create working directory and filename
sep_workdir = os.path.split(filepath)
workingdir = sep_workdir[0] + str('\\')
data_filename = '.'.join((sep_workdir[1].split('.'))[:-1])


##  Pull Header   Currently not used, but furture releases could use this data if needed
header = []
header_end = 4
cnt = 0
for aline in whole_file:
    if cnt == 0:
        break
    else:
        header.append(aline)
        cnt += 1

##############################
## DEFINITIONS ##
def open_data_file():
    '''
    NOT IMPLEMENTED YET, FOR FUTURE RELEASE
    
    Creates a window where the user will select the data file which will be converted
    '''
    Tk().withdraw()
    filepath = askopenfilename()
    return(filepath)

def sort_a_cycle(data):
    '''
    Input data: Lists of Lists, which is lines for a single cycle
    
    Takes data and will split the tabs out to return the charge and discharge data
    
    Will also return the  x y data point for efficiency.
    
    Returns: List of lists
    '''
    # Create empty strings
    voltageD = []
    capacityD = []
    voltageC = []
    capacityC = []
    
    # Parsing 
    for a_line in data:
        sep = a_line.split('\t')
        cycle_number = sep[1]
        if sep[9] == 'R':
            pass
        elif sep[9] == 'D':
            voltageD.append(float(sep[8]))
            capacityD.append(float(sep[5])*1000/mass_active_mat)
        elif sep[9] == 'C':
            voltageC.append(float(sep[8]))
            capacityC.append(float(sep[5])*1000/mass_active_mat)
    
    #Pull Efficiency Data and last charge capcityand discharge capcity 
    eff =[int(cycle_number),capacityC[-1]/capacityD[-1],capacityC[-1], capacityD[-1]]
    
    # Checking to see if the battery was put in backwards, if so it will swap the data to be consistant
    if eff[1] > 1:
        eff_opp = [int(cycle_number),capacityD[-1]/capacityC[-1],capacityD[-1], capacityC[-1]]
        return [capacityC,voltageC,capacityD,voltageD,eff_opp]
    else:
        return [capacityD,voltageD,capacityC,voltageC,eff]
        
def check_cycles():
    '''
    NOT IMPLEMENTED YET, FOR FUTURE RELEASE
    
    Pull last line of text file and check if the number equals entered by user equals the total number of cycles
    
    '''
    # Global Variables
    global num_of_cycles
    
    # User's First Input
    num_of_cycles = float(raw_input('Enter the number of cycles :'))
    
    # Pulling the split last line
    last_line = whole_file[-1].split('\t')
    last_cyc_num = int(last_line[1])
    
    # Checking to make sure the users input matches the data and asks for another entry if incorrect. 
    if last_cyc_num != num_of_cycles:
        print 'The number of cycles you enetered does not seem to match your data.\nPlease enter it again\nPlease remeber you must enter the TOTAL number of cycles in the text file.\nAn incorrect number can cause incorrect data to be exported'
        num_of_cycles = raw_input('Number of Cycles: ')
    else:
        pass

########################################################
##  CREATING MASTER DATA SET OF ALL CYCLES ##
master_data_set = []
data = whole_file[begin_of_data:] 
cycle = 1
single_cycl_data = []
for a_line in data:
    sep = a_line.split('\t')

    # Terminating condition     
    if int(cycle) > int(num_of_cycles):
        pass
    
    # Indicating last Cycle   
    elif int(sep[1]) == cycle+1: 
        
        master_data_set.append(sort_a_cycle(single_cycl_data))
        
        single_cycl_data = []
        single_cycl_data.append(a_line)
        # This should cause the terminating condition        
        cycle +=1
    # In the middle of the data, append to temp string
    elif int(sep[1]) == cycle:
        single_cycl_data.append(a_line)

####################################################
### CREATING IMAGE FILES ##
## Idividual images
if images == 'y':
    n=1
    for cycle in master_data_set:
        plot(cycle[0],cycle[1], label='Dischage',color='red')
        plot(cycle[2],cycle[3], label='Charge',color='blue')    
        # label and options here
        title('Voltage vs Charge Capacity for cycle '+str(n))
        grid(True)
        xlabel('mAh/g')
        ylabel('Voltage')
        legend(loc=7)
        # input extra options
        savefig(os.path.join(workingdir + data_filename +'_'+ 'Cycle_'+str(n)+'.png'))
        n += 1
        close()
    
    ## Combined Image
    n=1
    for cycle in master_data_set:
        plot(cycle[0],cycle[1],color='red')
        plot(cycle[2],cycle[3],color='blue')    
        # label and options here
        title('Voltage vs Charge Capacity for cycle 0-'+str(int(num_of_cycles)))
        grid(True)
        xlabel('mAh/g')
        ylabel('Voltage')
        n += 1
        
    savefig(os.path.join(workingdir + data_filename +'_'+ 'Cycle_0 to '+str(int(num_of_cycles))+'.png'))
    close()
    
    ## Efficiency Graph
    x=[]
    y=[] # Eff
    y2 = [] # Charge 
    y3 = [] # Discharge
    for a in master_data_set:
        x.append(int(a[4][0]))
        y.append(float(a[4][1]))
        y2.append(float(a[4][2]))
        y3.append(float(a[4][3]))
    fig, ax1 = subplots()
    ax1.plot(x,y2,'b-')
    ax1.plot(x,y3,'b',linestyle='--')
    ax1.set_xlabel('Cycle Number')
    ax1.set_ylabel('Capatcity (mAh/g)',color='b')
    for t1 in ax1.get_yticklabels():
        t1.set_color('b')
    
    ax2 = ax1.twinx()
    ax2.plot(x,y,'r-')
    ax2.set_ylabel('Efficieny',color='r')
    ax2.set_ybound(lower=0,upper=1)
    for t2 in ax2.get_yticklabels():
        t2.set_color('r')
    savefig(os.path.join(workingdir + data_filename +'_'+ 'Efficieny.png'))
    close()
else:
    pass


##############################################
###  EXCEL OUTPUT  ##

x=1
y=2
z=3

list1=[2.34,4.346,4.234]

book = xlwt.Workbook(encoding="utf-8")

sheet1 = book.add_sheet("Cycle Data")
sheet2 = book.add_sheet("Eff Data")

sheet1.write(0, 0, "Sample Name")
sheet1.write(1, 0, "Number of Cycles")
sheet1.write(2, 0, "Mass of Active Material (g)")
sheet2.write(0, 0, "Sample Name")
sheet2.write(1, 0, "Number of Cycles")
sheet2.write(2, 0, "Mass of Active Material (g)")

sheet1.write(0, 1, str(data_filename))
sheet1.write(1, 1, str(num_of_cycles))
sheet1.write(2, 1, str(mass_active_mat))
sheet2.write(0, 1, str(data_filename))
sheet2.write(1, 1, str(num_of_cycles))
sheet2.write(2, 1, str(mass_active_mat))


cnt_cyclenumber = 0
eff_data_x = []
eff_data_y = []
charge_cap_y = []
discharge_cap_y = []
for cyc in master_data_set:
    sheet1.write(3, cnt_cyclenumber, 'Capacity (D)')    
    sheet1.write(3, cnt_cyclenumber+1, 'Voltage (D)')
    sheet1.write(3, cnt_cyclenumber+2, 'Capacity (C)')
    sheet1.write(3, cnt_cyclenumber+3, 'Voltage (C)')
    sheet1.write(4, cnt_cyclenumber, 'mAh/g')
    sheet1.write(4, cnt_cyclenumber+1, 'V')
    sheet1.write(4, cnt_cyclenumber+2, 'mAh/g')
    sheet1.write(4, cnt_cyclenumber+3, 'V')    
    sheet1.write(5, cnt_cyclenumber, 'Cycle ' + str(cnt_cyclenumber/4+1))
    sheet1.write(5, cnt_cyclenumber+1, 'Cycle ' + str(cnt_cyclenumber/4+1))
    sheet1.write(5, cnt_cyclenumber+2, 'Cycle ' + str(cnt_cyclenumber/4+1))
    sheet1.write(5, cnt_cyclenumber+3, 'Cycle ' + str(cnt_cyclenumber/4+1))
    i = 5
    for fircol in cyc[0]:
        i +=1
        sheet1.write(i, cnt_cyclenumber, fircol)
    i = 5
    for seccol in cyc[1]:
        i +=1
        sheet1.write(i, cnt_cyclenumber+1, seccol)
    i = 5
    for thicol in cyc[2]:
        i +=1
        sheet1.write(i, cnt_cyclenumber+2, thicol)        
    i = 5
    for forcol in cyc[3]:
        i +=1
        sheet1.write(i, cnt_cyclenumber+3, forcol)
    eff_data_x.append(int(cyc[4][0]))
    eff_data_y.append(float(cyc[4][1]))
    charge_cap_y.append(float(cyc[4][2]))
    discharge_cap_y.append(float(cyc[4][3]))
    cnt_cyclenumber += 4
    
sheet2.write(4,0,'Cycle Number')
sheet2.write(4,1,'Efficiency')
sheet2.write(5,1,'%')
sheet2.write(4,2,'Charge Capacity')
sheet2.write(5,2,'mAh/g')
sheet2.write(4,3,'Discharge Capacity')
sheet2.write(5,3,'mAh/g')
i = 6
for x in eff_data_x:
    i += 1
    sheet2.write(i,0, x)
i = 6
for y in eff_data_y:
     i += 1
     sheet2.write(i, 1, y)
i = 6
for y in charge_cap_y:
    i +=1
    sheet2.write(i, 2, y)
i = 6
for y in discharge_cap_y:
    i +=1
    sheet2.write(i, 3, y)
     
book.save(os.path.join(workingdir + data_filename +'_'+ 'RawData.xls'))

#####################################
#End of Program

#####################################
## Main Program ##

### Set up Parameters
#clear = lambda: os.system('cls')
#value = True
#
### Main Event Case (if,elif) 
#while value is True:
#    
#    # Opening Statement
#    clear()
#    print 80*'*'+'\n\t\t\t Conversion Program for Cycler \n\t\t\t  Author: Steven Letourneau\n\t\t\t   Last Revised: ' + str(Pdate) + '\n\t\t\t\t VERSION: ' + str(Pversion) + '\n' +80*'*'+'\n'
#    if workingdir == '':
#        print 'Current working directory: None selected'
#    else:
#        print 'Current workind directory: '+ str(workingdir)
#    if data_filename == '':
#        print 'Current file: None selected'
#    else:
#        print 'Current file: ' + str(data_filename)
#
#    # Selection Menu
#    print '\n\nPlease select an option:'
#    print '\n 1 : Open a Data File (.txt)\n 2 : Convert Data\n 3 : Close Program'
#    options = {1: open_data_file, 2: convert_data, 3: close}
#
#    # User Input
#    op = Raw_input('(Selection: ')
#    if type(op) != 'int' or op > 3:
#        print ' Invalid opiton, try again '
#        pass 
#    elif op == 3:
#        value = False
#    elif op == 1:
#        pass 
#        #open_data_file()
#        # This needs to open the file, pull the working directory, and data filename.
#    elif op == 2:
#        #This needs to run the routine for conversion and images
#        pass
        
