import land_readin as land
import arbin_readin as arbin
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

'''
land_readin module:
    get_index(charge_type, cycle) -- Returns starting index value
    get_data(index, data_type) -- index input is a integer (index starting point) from get_index()
    data_type can be capacity, voltage, current
'''

'''
arbin_readin module:
  get_data(charge_type,data_type,cycle,filename):
    returns array of data for a certain data type, charge type and cycle
    data_type can be capacity or  voltage
    charge_type can be charge or discharge

'''

discharge_cap = land.get_data(charge_type = 'discharge', cycle = 2,data_type = 'capacity',filename = 'LAND_example.xlsx')
discharge_volt = land.get_data(charge_type = 'discharge', cycle = 2, data_type = 'voltage',filename = 'LAND_example.xlsx')

fig = plt.figure()
plt.plot(discharge_cap,discharge_volt)
plt.show()
