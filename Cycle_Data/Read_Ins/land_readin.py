import numpy as np
import pandas as pd

# Opens the excel file and first work sheet.
# test_time column used to find and count occurences of 'Charge CC' and 'Discharge CC' in work_sheet


def open_file(filename):

    excelFile = pd.ExcelFile(filename)  # Load excel file into memory
    work_sheet = pd.read_excel(excelFile, sheet_name=0, skiprows=2)
    test_time = np.array(work_sheet['TestTime'])
    
    return excelFile, work_sheet, test_time


def get_index(charge_type,cycle,filename):

    # Inputs are the cycle of interest (1, 5, 25, etc..) and charge type (charge or discharge)
    # Out put is a single number - The index value where the relevant data starts for that cycle

    excelFile, work_sheet, test_time = open_file(filename)

    if charge_type == 'charge':  # determines if for loop is counting charge or discharge
        chargeType = 'Charge CC'
    elif charge_type == 'discharge':
        chargeType = 'Discharge CC'

    index = 0
    charge_count = 0
    first_index = []  # The index value where the relevant data begins (capacity, voltage, etc)

    for i in test_time:
        if i == chargeType:
            charge_count += 1
            if charge_count == cycle:
                first_index.append(index + 2) # add 2 to account for blank spaces
                break
        else: pass
        index += 1

    first_index = first_index[0]

    return first_index, work_sheet


def get_data(data_type, charge_type, cycle, filename):

    index, work_sheet = get_index(charge_type, cycle, filename)
    if data_type == 'capacity':
        column_title = 'Capacity/mAh'
    if data_type == 'voltage':
        column_title = 'Voltage/V'
    if data_type == 'current':
        column_title = 'Current/mA'

    array = np.array(work_sheet[column_title])

    data = []
    for i in array[index:]:
        if i*0 == 0:
            data.append(i)
        else:
            break

    return data
