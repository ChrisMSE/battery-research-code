import pandas as pd
import numpy as np


def get_data(charge_type, data_type, cycle, filename):

    if data_type == 'voltage':
        column_name = 'Voltage(V)'
    elif data_type == 'capacity' and charge_type == 'charge':
        column_name = 'Charge_Capacity(Ah)'
    elif data_type == 'capacity' and charge_type == 'discharge':
        column_name = 'Discharge_Capacity(Ah)'

    data = []
    currCycle = 0
    excel_file = pd.ExcelFile(filename)
    work_sheets = excel_file.sheet_names
    for sheet in work_sheets[1:]:  # Have to skip the first worksheet in the Arbin files
        if currCycle > cycle: break
        ws = pd.read_excel(filename, sheet_name=sheet)
        value = np.array(ws[column_name])
        current = np.array(ws['Current(A)'])
        cycle_column = np.array(ws['Cycle_Index'])
        n = 0

        while currCycle <= cycle and n < len(cycle_column):
            if charge_type == 'charge':
                if(cycle_column[n] == cycle) and (current[n] < 0):
                    data.append(value[n])
                currCycle = cycle_column[n]
                n = n + 1
            elif charge_type == 'discharge':
                if(cycle_column[n] == cycle) and (current[n] > 0):
                    data.append(value[n])
                currCyccle = cycle_column[n]
                n = n + 1

    data = np.array(data)
    return data



