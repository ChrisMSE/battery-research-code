# Battery Data Code
I'll add some background information about the project soon, as well as my mental picture of it - Chris

# Using the Code:

- Download Battery_Data_Module.py and a.xls and make sure they are located in the same directory/folder.
- Make sure to have python installed (I believe it only works with Python 3 versions)
- Make sure to have the following modules installed:
	- pandas
		- from pandas: xlrd, XlsxWriter
	- numpy
	- matplotlib
	- scipy
	
	- All other used modules (os, time, random, sys) should be installed when Python is installed

- Open Battery_Data_Module.py
- When asked for the file to open just enter 'a' to open the a.xls file I've put in the repo
- When prompted for a label you can enter any string of text. 
- When prompted for the active weight you can enter any number > 0 (Typical values are around 0.005 if you want to see realistic results)
- Follow prompts and choose any of the types of graphs to create.

If for some reason its not working please email me at chrisjones4@u.boisestate.edu


# LAND_practice.py

Starting work on code to separate the data from the LAND_example spreadsheet into individual cycles
